# IA_PEC3

## Nombre
Aprendizaje en videojuegos \
UOC - IA - PEC3 \
Actividad 1: Rollerball

## Descripción
Proyecto de la actividad 1 de la PEC3 de Inteligencia Artificial para videojuegos.

En este proyecto, aprendemos a usar la herramienta ML-Agents de Unity para entrenar agentes mediante técnicas de refuerzo o `reinforcement learning`. En esta primera actividad debemos seguir el tutorial que nos ofrece unity para configurar la herramienta, el agente, iniciar el aprendizaje y mostrar el resultado usando el modelo de inteligencia artificial generado.
Debemos enseñar a nuestro agente (en este caso una esfera) a llegar a un lugar en concreto, el target (en nuestro caso un cubo).

## Actividad: Rollerball
![Rollerball](/Assets/Resources/Images/Rollerball.png)\
*Rollerball*

Para esta actividad se ha creado un agente (Esfera) que ha aprendido mediante reinforcement learning a moverse hacia su objetivo (Cubo). Para llegar a este resultado se le ha enseñado siguiendo el metodo PPO a moverse hacia su objetivo dandole un refuerzo positivo cuando lo alcanzaba y uno negativo cuando se salia de los limites de la escena. Las observaciones son su posición, la de su objetivo, y las velocidades de los ejes x/z.
Las acciones que tiene el agente son moverse en el eje de las x y moverse en el eje de las z. En cualquiera de las dos se le aplica una fuerza al agente que hace que se mueva.
A continuación se muestra el código donde se ve el comportamiento del agente.

OnEpisodeBegin -  Cada vez que se inicia un episodio de aprendizaje:
```csharp
    public override void OnEpisodeBegin()
    {
        // If the Agent fell, zero its momentum
        if (this.transform.localPosition.y < 0)
        {
            this.rBody.angularVelocity = Vector3.zero;
            this.rBody.velocity = Vector3.zero;
            this.transform.localPosition = new Vector3(0, 0.5f, 0);
        }

        // Move the target to a new spot
        Target.localPosition = new Vector3(Random.value * 8 - 4,
                                           0.5f,
                                           Random.value * 8 - 4);
    }
```
CollectObservations -  Las observaciones que tiene el agente:
```csharp
    public override void CollectObservations(VectorSensor sensor)
    {
        // Target and Agent positions
        sensor.AddObservation(Target.localPosition);
        sensor.AddObservation(this.transform.localPosition);

        // Agent velocity
        sensor.AddObservation(rBody.velocity.x);
        sensor.AddObservation(rBody.velocity.z);
    }
```
OnActionReceived - Cada vez que el agente decide hacer una acción:
```csharp
    public override void OnActionReceived(ActionBuffers actionBuffers)
    {
        // Actions, size = 2
        Vector3 controlSignal = Vector3.zero;
        controlSignal.x = actionBuffers.ContinuousActions[0];
        controlSignal.z = actionBuffers.ContinuousActions[1];
        rBody.AddForce(controlSignal * forceMultiplier);

        // Fell off platform
        if (this.transform.localPosition.y < 0)
        {
            SetReward(-1.0f);
            EndEpisode();
        }
    }
```
Heuristic - Para controlar nosotros al agente y ver que todo funciona:
```csharp
    public override void Heuristic(in ActionBuffers actionsOut)
    {
        var continuousActionsOut = actionsOut.ContinuousActions;
        continuousActionsOut[0] = Input.GetAxis("Horizontal");
        continuousActionsOut[1] = Input.GetAxis("Vertical");
    }
```
Para determinar que el agente ha logrado llegar a su objetvo se ha hecho mediante un trigger collider:
```csharp
    private void OnTriggerEnter(Collider other)
    {
        // Check if the collider is the target
        if (other.gameObject.CompareTag("Target"))
        {
            SetReward(1.0f);
            EndEpisode();
        }
    }
```

Así pues, con el entorno predefinido, el agente realizando solo 2 acciones (moverse en los dos ejes), observando su velocidad, posición y la posición del objetivo y mediante refuerzos positivos y negativos hemos conseguido que el agente pueda mantenerse en la plataforma yendo a buscar siempre su objetivo. Para acelerar el proceso se ha duplicado el entorno de aprendizaje varias veces para que el agente obtuviera más casuisticas de forma simultanea.

![Training Areas](/Assets/Resources/Images/TrainingAreas.png "Training Areas")\
*Training Areas*

## Video
A continuación un video demostración del agente:\
https://youtu.be/UzhiWO-9-lY